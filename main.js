function startDrag(e) {
  if (!e) {
    var e = window.event;
  }
  if(e.preventDefault) e.preventDefault();
  targ = e.target ? e.target : e.srcElement;
  if (targ.className != 'dragme') {return};
  offsetX = e.clientX;
  offsetY = e.clientY;
  if(!targ.style.left) { targ.style.left='0px'};
  if (!targ.style.top) { targ.style.top='0px'};
  coordX = parseInt(targ.style.left);
  coordY = parseInt(targ.style.top);
  drag = true;
  document.onmousemove=dragDiv;
  return false;

}
function dragDiv(e) {
  if (!drag) {return};
  if (!e) { var e= window.event};
  targ.style.left=coordX+e.clientX-offsetX+'px';
  targ.style.top=coordY+e.clientY-offsetY+'px';
  return false;
}
function stopDrag() {
  drag=false;
}

window.onload = function() {
  document.onmousedown = startDrag;
  document.onmouseup = stopDrag;
}

//DRAG AND DROP
var loadFile = function(event) {
  var img = document.createElement('img');
  img.src = URL.createObjectURL(event.target.files[0]);
  img.setAttribute('class', 'dragme');
  document.getElementById('created-images').appendChild(img);
  down.innerHTML = "Image Element Added.";
};

//Carroussel
var myCarousel = new Krousel(document.querySelector('.slider'),{

  // Change where arrows are attached (default is the target)
  appendArrows: null,

  // Change where the navigation dots are attached
  appendDots: null,

  // enable or disable arrows
  arrows: true,

  // Auto play the carousel
  autoplay: false,

  // Change the interval at which autoplay change slide
  autoplaySpeed: 3000,

  // Display or Hide dots
  dots: true,

  // Enable or disable infinite behavior
  infinite: true,

  // Customize the "next" arrow
  nextArrow: null,

  // pause autoplay when a slide is hovered,
  pauseOnHover: true,

  // Customize the "previous" arrow
  prevArrow: null,

  // breakpoints config
  responsive: null,

  // Number of slide to show at once
  slidesToShow: 1,

  // Number of slide to scroll when clicking on arrow
  slidesToScroll: 1,

  // transition speed when changing slide
  speed: 300,

  // Change transition type when changing slide
  // NOTE: transition 'fade' disable options slidesToShow and slidesToScroll
  transition: 'slide',

});

